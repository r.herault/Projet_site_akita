# Projet site Akita
Exécuter le creer_html.py pour générer les pages :
```
./creer_html.py
```
**OU**
```
python creer_html.py
```

## Réalisé avec

* [Jinja](http://jinja.pocoo.org/) - Utilisé pour générer les pages HTML
* [MDBootstrap](https://mdbootstrap.com/material-design-for-bootstrap/) - Le template du site


## Développeur

* **Romain HERAULT** - *Master* - [r.herault](http://rherault.fr)

## Liens
[katsuhiro-kensha.fr](https://katsuhiro-kensha.fr)