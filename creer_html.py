#!/usr/bin/python3

from jinja2 import Environment, FileSystemLoader # ne pas modifier
from photos import *

#from  import *

def creer_html(fichier_template, fichier_sortie,**infos):
    """
    Cette fonction génère automatiquement un fichier à partir d'un
    template et d'informations
    paramètres :
        fichier_template : un fichier template (template HTML par exemple)
        fichier_sortie : le nom du fichier généré
        **infos : un nombre indéfini de paramètres qu'il suffit de nommer
    return : -
    """
    env=Environment(loader=FileSystemLoader('.'),trim_blocks=True)
    template=env.get_template(fichier_template)
    html=template.render(infos)
    f = open(fichier_sortie, 'w')
    f.write(html)
    f.close()

creer_html('index.html', 'genere/index.html')
creer_html('accueil.html', 'genere/accueil.html')
creer_html('males.html', 'genere/males.html')
creer_html('femelles.html', 'genere/femelles.html')
creer_html('chiots.html', 'genere/chiots.html')
creer_html('contact.html', 'genere/contact.html')
creer_html('album.html', 'genere/album.html',
            repertoire = "photos/album/",
            album = album)
creer_html('hokusai_photos.html', 'genere/hokusai_photos.html',
            repertoire = 'photos/Hokusai/',
            photos_hokusai = photos_hokusai)
creer_html('nodaiko_photos.html', 'genere/nodaiko_photos.html',
            repertoire = 'photos/Nodaiko/',
            photos_nodaiko = photos_nodaiko)
creer_html('toganEtTakeo_photos.html', 'genere/toganEtTakeo_photos.html',
            repertoire = 'photos/Togan&Takeo/',
            photos_toganEtTakeo = photos_toganEtTakeo)
creer_html('akemi_photos.html', 'genere/akemi_photos.html',
            repertoire = 'photos/Akemi/',
            photos_akemi = photos_akemi)
creer_html('tsukiyo_photos.html', 'genere/tsukiyo_photos.html',
            repertoire = 'photos/Tsukiyo/',
            photos_tsukiyo = photos_tsukiyo)
creer_html('kyokkumi_photos.html', 'genere/kyokkumi_photos.html',
            repertoire = 'photos/Kyokkumi/',
            photos_kyokkumi = photos_kyokkumi)
creer_html('okinawa_photos.html', 'genere/okinawa_photos.html',
            repertoire = 'photos/Okinawa/',
            photos_okinawa = photos_okinawa)
creer_html('wakame_photos.html', 'genere/wakame_photos.html',
            repertoire = 'photos/Wakame/',
            photos_wakame = photos_wakame)
creer_html('chiots_photos.html', 'genere/chiots_photos.html',
            repertoire = 'photos/chiots/',
            photos_chiots = photos_chiots)
